"""
URL configuration for projectedjango project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path
from django.views.generic import RedirectView, TemplateView

from AplicacioDjango.views import AddPlataformaView, \
    ObtenirPlataformaAleatoria, ObtenirPlataformaAleatoriaSenseCensura, ObtenirPlataformesUsuari, \
    ObtenirPlataformaAleatoriaUsuari, AddOpinioView, AfegirPlataforma, \
    AssociarUsuariPlataforma, AfegirOpinio, CensurarOpinio, EsborrarOpinio, RegisterView


urlpatterns = [
    # 'TemplateView' es una vista generica de Django que s'utilitza per mostrar una plantilla HTML
    # 'as_view' el que fa es crear una funcio de vista que Django necessita per gestionar les solicitud HTTP
    path("", TemplateView.as_view(template_name="home.html"), name="home"),
    path('register/', RegisterView.as_view(), name='register_view'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('admin/', admin.site.urls, name='admin'),
    path('addPlataforma/', AddPlataformaView.as_view(), name='add_plataforma'),
    path('afegimPlataforma/', AfegirPlataforma.as_view(), name='afegir_plataforma'),
    path('plataformaUser/<str:nom_usuari>/<int:plataforma_id>', AssociarUsuariPlataforma.as_view(), name='associar_usuari_plataforma'),
    path('plataformaRandom/', ObtenirPlataformaAleatoria.as_view(), name='obtenir_plataforma_random'),
    path('plataformaRandomCensura/', ObtenirPlataformaAleatoriaSenseCensura.as_view(), name='obtenir_plataforma_random_sense_censura'),
    path('getPlataformes/<str:nom_usuari>', ObtenirPlataformesUsuari.as_view(), name='obtenir_plataformes_usuari'),
    path('getPlataforma/<str:nom_usuari>', ObtenirPlataformaAleatoriaUsuari.as_view(), name='obtenir_plataforma_aleatoria_usuari'),
    path('addOpinio/', AddOpinioView.as_view(), name='add_opinio'),
    path('addOpinio/<int:usuari_id>/<int:plataforma_id>', AfegirOpinio.as_view(), name='afegir_opinio'),
    path('putinOpinio/<int:opinio_id>', CensurarOpinio.as_view(), name='censurar_opinio'),
    path('ReportOpinio/<int:plataforma_id>/<str:nom_usuari>', EsborrarOpinio.as_view(), name='borrar_opinio'),
]

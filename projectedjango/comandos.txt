Los siguientes comandos SE REALIZAN en la RAÍZ del proyecto: "projectedjango"

1. En el archivo "settings.py" de la raíz del proyecto añadir en "INSTALLED_APPS":
    - 'rest_framework',

2.1 Crear nuestra app:
    - Comando: python manage.py startapp nombre_app
    - Ejemplo: python manage.py startapp AplicacioDjango

2.2 Creamos app users:
    - python manage.py startapp Users

3. Añadir en el archivo "settings.py" en "INSTALLED_APPS":
    - 'AplicacioDjango',  (el nombre de nuestra aplicación)
    - 'Users',

4. Una vez tenemos realizados los modelos en el archivo "models.py" de la app, ejecutamos el comando:
    - python manage.py makemigrations
    Este comando lo que hace es crear el archivo '0001_initial.py' dentro de la carpeta 'migrations'

5. A continuación, confirmamos que en el archivo settings tengamos esta configuración:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'plataforma',
            'USER': 'root',
            'PASSWORD': 'super3',
            'HOST': 'localhost',
            'PORT': '3306',
        }
    }

6. Ejecutamos el comando:
    - python manage.py migrate
    Este comando lo que hace es pasar los datos del archivo 'migrations/0001_initial.py' a la BB.DD.

7. Creamos el superusuario:
    - py manage.py createsuperuser
    En nuestro caso hemos puesto que el usuario es "root" y contraseña "super3"
from django.apps import AppConfig


class AplicaciodjangoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'AplicacioDjango'

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import Opinions, CustomUser


# Registrem els models en l'administracio de Django
class OpinionsAdmin(admin.ModelAdmin):
    list_display = ['Descripcio', 'ValoracioPositiva', 'OpinioCensurada']


admin.site.register(Opinions, OpinionsAdmin)


class CustomUserAdmin(UserAdmin):
    list_display = ['username', 'email', 'username', 'first_name', 'last_name', 'num_opinions']


admin.site.register(CustomUser, CustomUserAdmin)

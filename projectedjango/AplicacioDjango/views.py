import random
from datetime import datetime

from django.contrib.auth import authenticate, login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic.base import View

from AplicacioDjango.forms import FormulariDeRegistre
from AplicacioDjango.models import Plataformes, Opinions, CustomUser


# Creem las vistes

# Amb el LoginRequiredMixin el que fem es que no es pugui accedir a la URL si no s'esta loguejat
# IMPORTANT: Si tenim una vista en la qual hi ha un formulari que fa un get, i quan presionem el botó del form fara un post,
# llavors la primera consulta sera un get i la segona un post. En canvi, si directament fem un post passant els parametres
# per URL, llavors el metode que hem de posar es 'get'

# En aquest cas, utilitzem la mateixa funcio per fer el GET i el POST. Aixo es util quan la vista es relativament simple
# i ambdos metodes comparteixen la majoria de la logica. Facilita la comprensio i manteniment del codi, ja que totes les
# operacions relacionades amb una vista es troben en un sol lloc. Pot ser mes eficient per linies de codi i organitzacio
class RegisterView(View):
    def get(self, request):
        # Com que tenim un CustomUser, hem de crear el que hi ha a l'arxiu 'forms.py' per a que utilitzi el CustomUser
        # en comptes del User de Django
        form = FormulariDeRegistre()
        return render(request, 'registration/register.html', {'form': form})

    def post(self, request):
        form = FormulariDeRegistre(request.POST)
        if form.is_valid():
            # Guarda les dades del formulari en la BB.DD.
            form.save()
            # Obte el nom d'usuari ingresat en el formulari
            username = form.cleaned_data.get('username')
            # Obte la contrasenya ingressada en el formulari
            password = form.cleaned_data.get('password1')
            # Autentica l'usuari que s'acaba de registrar (verifica si l'usuari i contrasenya son valids en la BB.DD.)
            user = authenticate(username=username, password=password)
            # Inicia sessio amb l'usuari que s'acaba d'autenticar
            login(request, user)

            return redirect('login') # Si el formulari es valid, ens redireccionara al login

        # Si l'usuari no es valid, es torna a renderitzar la pagina de registre
        return render(request, 'registration/register.html', {'form': form})


# En aquesta vista, utilitzem funcions separades pel GET i el POST. Es util quan els comportaments de GET i POST son
# significativament diferents o si el post gestiona una logica mes complexa. Millora la llegibilitat i comprensio del codi
# en separar clarament la logica de la gestio de solicituds del GET i POST. Tambe pot facilitar la reutilitzacio del codi
# i manteniment a llarg termini, especialment si la vista necessita ser expandida amb logica addicional
class AddPlataformaView(LoginRequiredMixin, View):
    def get(self, request):
        return render(request, 'addplataforma.html')


class AfegirPlataforma(LoginRequiredMixin, View):
    def post(self, request):
        try:
            if request.method == 'POST':
                # Recuperem les dades introduïdes en el form
                nom_plataforma = request.POST.get('nomPlataforma')
                data_plataforma = request.POST.get('dataPlataforma')

                # Comprovem que la plataforma que s'intenta afegir, no existeixi, si existeix, ens mostra missatge
                if Plataformes.objects.filter(Nom=nom_plataforma).exists():
                    return render(request, 'mostrarmissatge.html', {'missatge': "Aquesta plataforma ja existeix."})

                # Creem una plataforma amb els atributs que ens arriben per la request (que son els que s'han indicat en el form)
                plataforma = Plataformes(Nom=nom_plataforma, Data=data_plataforma)
                # Hacemos un save de la plataforma
                plataforma.save()

                missatge = f'Plataforma afegida correctament: {plataforma}.'

        # Si el try no funciona, llança un missatge d'exepcio
        except Exception as e:
            missatge = f"Hi ha hagut un error a l'intentar afegir la plataforma {plataforma}."

        # Si el try funciona, mostra el següent missatge:
        return render(request, 'mostrarmissatge.html', {'missatge': missatge})


class AssociarUsuariPlataforma(LoginRequiredMixin, View):
    def get(self, request, nom_usuari, plataforma_id):
        try:
            # Obtenim l'usuari pel seu nom d'usuari
            usuari = get_object_or_404(CustomUser, username=nom_usuari)
            # Obtenim la plataforma pel seu ID
            plataforma = get_object_or_404(Plataformes, id=plataforma_id)

            # Comprovem si l'usuari ja està associat a la plataforma:
            if usuari in plataforma.Usuaris.all():
                missatge = f"L'usuari {usuari} ja està associat a la plataforma {plataforma}."
                return render(request, 'mostrarmissatge.html', {'missatge': missatge})

            # Associem l'usuari amb la plataforma
            plataforma.Usuaris.add(usuari)

            missatge = f'Usuari {usuari} associat correctament amb la plataforma {plataforma}.'

        # Si el try no funciona, llança un missatge
        # Tambe podriem llançar una excepcio amb error 500 d'aquesta forma:
        # raise HttpResponse(content="Hi ha hagut un error a l'intentar associar l'usuari amb la plataforma", status=500)
        except Exception as e:
            missatge = f"Hi ha hagut un error a l'intentar associar l'usuari {usuari} amb la plataforma amb id {plataforma_id}."

        # Si el try funciona, mostra el missatge:
        return render(request, 'mostrarmissatge.html', {'missatge': missatge})


class ObtenirPlataformaAleatoria(LoginRequiredMixin, View):
    def get(self, request):
        # Obtenim totes les plataformes
        plataformes = Plataformes.objects.all()
        # Seleccionem una plataforma de forma aleatoria
        plataforma_aleatoria = random.choice(plataformes)

        context = {'plataformaAleatoria': plataforma_aleatoria}

        return render(request, 'plataformaleatoria.html', context=context)


class ObtenirPlataformaAleatoriaSenseCensura(LoginRequiredMixin, View):
    def get(self, request):
        # Obtenim totes les plataformes que tinguin la opinio sense censurar
        # Amb filter obtenim varies plataforems; amb el get nomes una
        plataformas_amb_opinio_no_censurada = Plataformes.objects.filter(opinions__OpinioCensurada=False)
        # Seleccionem una plataforma de forma aleatoria
        plataforma_aleatoria = random.choice(plataformas_amb_opinio_no_censurada)

        context = {'plataformaAleatoria': plataforma_aleatoria}

        return render(request, 'plataformaaleatoriasensecensura.html', context=context)


class ObtenirPlataformesUsuari(LoginRequiredMixin, View):
    def get(self, request, nom_usuari):
        try:
            # Obtenim tots els usuaris que tinguin el nom 'nom_de_usuario'
            usuari = CustomUser.objects.get(username=nom_usuari)
            # Obtenim totes les plataformes de l'usuari (IMPORTANT: el camp 'plataformes_users' ha de ser el 'related_name' del camp del model)
            plataformes = usuari.plataformes_users.all()

            # Comprovem si l'usuari té plataformes associades
            if len(plataformes) == 0:
                return render(request, 'mostrarmissatge.html',
                              {'missatge': f"L'usuari {usuari} no té cap plataforma associada."})

            context = {'plataformes': plataformes}

        # Excepcio que es llança si l'usuari no existeix
        except CustomUser.DoesNotExist:
            return render(request, 'mostrarmissatge.html', {'missatge': "L'usuari introduït no existeix."})

        return render(request, 'plataformesusuari.html', context=context)


class ObtenirPlataformaAleatoriaUsuari(LoginRequiredMixin, View):
    def get(self, request, nom_usuari):
        try:
            # Obtenim tots els usuaris que tinguin el nom 'nom_de_usuario'
            usuari = CustomUser.objects.get(username=nom_usuari)

            # Obtenim totes les plataformes de l'usuari (IMPORTANT: el camp 'plataformes_users' ha de ser el 'related_name' del camp del model)
            plataformes = usuari.plataformes_users.all()

            # Comprovem si l'usuari té plataformes associades
            if len(plataformes) == 0:
                return render(request, 'mostrarmissatge.html',
                              {'missatge': f"L'usuari {usuari} no té cap plataforma associada."})

            # Seleccionem una plataforma de forma aleatoria
            plataforma_aleatoria = random.choice(usuari.plataformes_users.all())

            context = {'plataformaAleatoria': plataforma_aleatoria}

        # Excepcio que es llança si l'usuari no existeix
        except CustomUser.DoesNotExist:
            return render(request, 'mostrarmissatge.html', {'missatge': "L'usuari introduït no existeix."})

        return render(request, 'plataformaaleatoriausuari.html', context=context)


class AddOpinioView(LoginRequiredMixin, View):
    def get(self, request):
        # Recuperem tots els usuaris
        usuaris = CustomUser.objects.all()
        # Recuperem totes les plataformes
        plataformes = Plataformes.objects.all()

        context = {'usuaris': usuaris, 'plataformes': plataformes}

        return render(request, 'addopinio.html', context=context)


class AfegirOpinio(LoginRequiredMixin, View):
    def post(self, request, usuari_id, plataforma_id):
        try:
            # Obtenim l'usuari per la seva ID
            usuari = get_object_or_404(CustomUser, id=usuari_id)
            # Obtenim la plataforma per la seva ID
            plataforma = get_object_or_404(Plataformes, id=plataforma_id)
            # Associem l'usuari amb la plataforma
            plataforma.Usuaris.add(usuari)

            if Opinions.objects.filter(Plataforma=plataforma, Usuari=usuari).exists():
                missatge = f"L'usuari {usuari.username} ja té opinió per la plataforma {plataforma}."
                return render(request, 'mostrarmissatge.html', {'missatge': missatge})

            elif request.method == 'POST':
                # Recuperem les dades introduïdes en el form
                descripcio_opinio = request.POST.get('descripcioOpinio')
                valoracio_opinio = request.POST.get('valoracioOpinio')
                censurada_opinio = request.POST.get('opinioCensurada')

                # Per debugar les variables i el tipus de variables
                print("Valor de valoracio_opinio:", valoracio_opinio)
                print("Valor de valoracio_opinio (type):", type(valoracio_opinio))
                print("Valor de censurada_opinio:", censurada_opinio)
                print("Valor de censurada_opinio (type):", type(censurada_opinio))

                if valoracio_opinio == 'on':
                    valoracio_opinio = True
                else:
                    valoracio_opinio = False

                if censurada_opinio == 'on':
                    censurada_opinio = True
                else:
                    censurada_opinio = False

                # Creem una opinio amb les dades del form
                opinio = Opinions(Descripcio=descripcio_opinio, ValoracioPositiva=valoracio_opinio, OpinioCensurada=censurada_opinio, Plataforma=plataforma, Usuari=usuari)
                # Fem un save de la opinio per guardar-la amb els canvis fets en la BB.DD.
                opinio.save()

                # Agafem el num d'opinions de l'usuari i l'augmentem en 1
                usuari.num_opinions += 1
                # Fem un save de l'usuari per guardar-lo amb els canvis fets en la BB.DD.
                usuari.save()

                # Agafem la data actual
                now = datetime.now()
                # Li donem format a la data actual
                data = now.strftime("%d/%m/%Y")
                missatge = f"{descripcio_opinio}"

            else:
                missatge = f"El mètode ha de ser POST i estàs utilitzant {request.method}."
                return render(request, 'mostrarmissatge.html', {'missatge': missatge})

        # Excepcio que es llança si hi ha algun error
        except Exception as e:
            missatge = f"Hi ha hagut un error a l'intentar afegir l'opinió."

        # Si el try funciona, mostra el següent missatge:
        return render(request, 'mostraropinio.html',
                      {'missatge': missatge, 'user': usuari.username, 'plataforma': plataforma, 'data': data})


class CensurarOpinio(LoginRequiredMixin, View):
    def get(self, request, opinio_id):
        try:
            # Obtenim l'opinio per la seva ID
            opinio = Opinions.objects.get(id=opinio_id)

            if opinio.OpinioCensurada == True:
                missatge = f"La opinió indicada ja està censurada."
            else:
                # Li canviem el valor de la columna OpinioCensurada a True
                opinio.OpinioCensurada = True
                # Guardem la opinio
                opinio.save()
                missatge = f"Opinió censurada correctament."

        # Excepcio que es llança si la opinio no existeix
        except Opinions.DoesNotExist:
            return render(request, 'mostrarmissatge.html',
                          {'missatge': "La opinió indicada no existeix i, per tant, no es pot censurar."})

        return render(request, 'mostrarmissatge.html', {'missatge': missatge})


class EsborrarOpinio(LoginRequiredMixin, View):
    def get(self, request, plataforma_id, nom_usuari):
        try:
            # Obtenim l'usuari a traves del seu nom
            usuari = CustomUser.objects.get(username=nom_usuari)

            # Obtenim la plataforma a traves de su ID
            plataforma = Plataformes.objects.get(id=plataforma_id)

            # Obtenim l'opinio que tingui l'id de plataforma i user passats per parametre
            opinio = Opinions.objects.get(Plataforma=plataforma, Usuari=usuari)

            # Eliminem l'opinio
            opinio.delete()

            # Eliminem plataforma-usuari de la taula intermitja
            plataforma.Usuaris.remove(usuari)

            # Agafem el num d'opinions de l'usuari i li disminuim en 1
            usuari.num_opinions -= 1
            # Fem un save de l'usuari per guardar-lo amb els canvis fets en la BB.DD.
            usuari.save()

        # Excepcio que es llança si l'usuari no existeix
        except CustomUser.DoesNotExist:
            return render(request, 'mostrarmissatge.html', {'missatge': "L'usuari introduït no existeix."})
        # Excepcio que es llança si la plataforma no existeix
        except Plataformes.DoesNotExist:
            return render(request, 'mostrarmissatge.html', {'missatge': "La plataforma introduïda no existeix."})
        # Excepcio que es llança si la opinio no existeix
        except Opinions.DoesNotExist:
            return render(request, 'mostrarmissatge.html',
                          {'missatge': "No s'ha trobat cap opinió associada a la plataforma i usuari indicat."})
        # Excepcio que es llança quan es produeix qualsevol altre error
        except Exception as e:
            return render(request, 'mostrarmissatge.html',
                          {'missatge': "S'ha produït un error a l'intentar esborrar l'opinió."})

        return render(request, 'mostrarmissatge.html', {
            'missatge': f"L'opinió de l'usuari {usuari} i la plataforma {plataforma} s'ha esborrat correctament."})

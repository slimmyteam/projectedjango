from django import forms
from django.contrib.auth.forms import UserCreationForm
from AplicacioDjango.models import CustomUser


# Aquesta classe hereta de 'UserCreationForm' que es una classe de Django que gestiona la creacio d'usuaris,
# la validacio de contrasenyes i la creacio de camps de contrasenya
class FormulariDeRegistre(UserCreationForm):
    # Al formulari basic de UserCreationForm (que conte els camps de username i password + confirmacio password),
    # li afegim el camp e-mail, amb un EmailField (perque nomes accepti correus electronics valids)
    email = forms.EmailField(max_length=200, help_text='Required')

    # La classe Meta s'utilitza per proporcionar metadades sobre el formulari, com quin model s'utilitzara per a crear
    # el formulari. En aquest cas, s'utilitzara el model CustomUser, que es la classe model que hem personalitzat
    # En 'fields' s'especifiquen quins camps del model CustomUser s'inclouran en el formulari
    class Meta:
        model = CustomUser
        fields = ('username', 'email', 'password1', 'password2')

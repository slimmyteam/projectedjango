from django.contrib.auth.models import AbstractUser, Permission, Group
from django.db import models
from datetime import date


# Creem un model CustomUser per afegir a la clase model default de Django el camp 'num_opinions'
# IMPORT: S'ha d'afegir aquesta classe a l'arxiu 'settings.py' aixi: AUTH_USER_MODEL = 'AplicacioDjango.CustomUser'
class CustomUser(AbstractUser):
    num_opinions = models.IntegerField(default=0)


class Plataformes(models.Model):
    Nom = models.CharField(max_length=30, null=False, default="")
    Data = models.DateField(default=date.today, blank=True)
    # Relación ManyToMany (amb el related_name, indiquem la taula amb la qual hi haura bidireccionalitat)
    Usuaris = models.ManyToManyField(CustomUser, related_name='plataformes_users')

    # Aixo s'afegeix per a que en el Panell de l'Admin, al afegir una opinio, apareixi el nom de la plataforma en el camp 'Plataforma'
    # sino apareixeria: 'PlataformaObject (1)'. Es el "toString" de la classe Plataformes
    def __str__(self):
        return self.Nom


class Opinions(models.Model):
    Descripcio = models.CharField(max_length=250, null=False, default="")
    ValoracioPositiva = models.BooleanField(default=False)
    OpinioCensurada = models.BooleanField(default=False)
    # Relacio ManyToOne (moltes opinions poden tenir nomes una plataforma)
    Plataforma = models.ForeignKey(Plataformes, on_delete=models.CASCADE)
    # Relacio ManyToOne (moltes opinions poden tenir nomes un usuari)
    Usuari = models.ForeignKey(CustomUser, on_delete=models.CASCADE)

# 📑 Aplicación de gestión de plataformas (Netflix, HBO, Amazon Prime...) y opiniones 📑

Aplicación en Python y Django que realiza operaciones CRUD (MySQL) e incluye _login_ y registro.  

## 📄 Descripción
Proyecto realizado para la asignatura "Acceso a datos" desarrollado durante el segundo año de Grado Superior de Desarrollo de Aplicaciones Multiplataforma + Perfil videojuegos y ocio digital (DAMvi).

## 💻 Tecnologías
![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)
![Django](https://img.shields.io/badge/django-%23092E20.svg?style=for-the-badge&logo=django&logoColor=white)
![MySQL](https://img.shields.io/badge/mysql-4479A1.svg?style=for-the-badge&logo=mysql&logoColor=white)

## ⚙️ Funcionamiento
El funcionamiento se puede ver en el documento de pruebas del proyecto adjuntado a continuación: [Documento de pruebas](https://gitlab.com/slimmyteam/python/projectedjango/-/blob/main/projectedjango/Document_de_proves_Django.pdf).

## ©️ Desarrolladores
- Ethan Corchero Martín.
- Ana María Gómez León.
- Selene Milanés Rodríguez.